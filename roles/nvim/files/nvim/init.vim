set encoding=utf-8
set number
set splitbelow
set splitright

set ruler
set cursorline

set expandtab
set tabstop=2
set shiftwidth=2
inoremap <silent> jj <ESC>
