SHELL=/bin/bash
.DEFAULT_GOAL := all

all:
	( \
		source .venv/bin/activate; \
		ansible-playbook -i hosts site.yml \
	)

nodenv:
	( \
		source .venv/bin/activate; \
		ansible-playbook -i hosts site.yml --tags "nodenv" \
	)
